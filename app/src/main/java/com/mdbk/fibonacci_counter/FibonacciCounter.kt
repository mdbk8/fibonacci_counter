package com.mdbk.fibonacci_counter

import java.math.BigInteger

class FibonacciCounter {

    private var first: BigInteger = BigInteger.ZERO
    private var second: BigInteger = BigInteger.ONE
    private var fib: BigInteger = BigInteger.ONE

    fun fibonacciSeq(positionInSequence: Long): BigInteger {
        resetFields()

        return when {
            positionInSequence <= 0 -> BigInteger.ZERO
            positionInSequence == 1L -> BigInteger.ONE
            else -> {
                for (index in 2..positionInSequence) {
                    fib = first.add(second)
                    first = second
                    second = fib
                }

                fib
            }
        }
    }

    private fun resetFields() {
        first = BigInteger.ZERO
        second = BigInteger.ONE
        fib = BigInteger.ONE
    }
}
