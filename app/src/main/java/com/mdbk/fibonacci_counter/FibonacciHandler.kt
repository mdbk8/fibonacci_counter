package com.mdbk.fibonacci_counter

import android.os.Handler
import android.os.HandlerThread
import android.os.Message
import android.util.Log
import android.widget.TextView

import java.math.BigInteger

class FibonacciHandler : Handler() {

    @Volatile
    var position = 0L
        private set

    private var label: TextView? = null
    private val threadHandler: Handler
    private val counter = FibonacciCounter()
    private val fibonacciThread: HandlerThread

    private val backgroundJob = Runnable {
        val fibonacci = counter.fibonacciSeq(position++)
        val uiHandler = this@FibonacciHandler
        uiHandler.sendMessage(uiHandler.obtainMessage(2, fibonacci))
    }

    init {
        Log.d("FibonacciHandler", "FibonacciHandler's constructor")
        fibonacciThread = HandlerThread("fibonacci").also { it.start() }
        threadHandler = Handler(fibonacciThread.looper)
    }

    override fun handleMessage(msg: Message) {
        val fibonacci = msg.obj as BigInteger
        label!!.text = fibonacci.toString()
        Log.d("MainActivity", "printing fibonacci: " + fibonacci)
        threadHandler.sendMessageDelayed(message(), 1000)
    }

    fun startCounting(label: TextView, startingPos: Long) {
        position = startingPos
        this.label = label
        threadHandler.sendMessage(message())
        Log.d("MainActivity", "start counting")
    }

    fun stopCounting() {
        this.label = null
        threadHandler.removeMessages(1)
        Log.d("MainActivity", "stop counting")
    }

    private fun message() = Message.obtain(threadHandler, backgroundJob).also { it.what = 1 }

    fun cleanUp() = fibonacciThread.quit()
}
