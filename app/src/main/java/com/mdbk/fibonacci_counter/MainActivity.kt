package com.mdbk.fibonacci_counter

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var myHandler: FibonacciHandler? = null
    private var startingPos: Long = 0
    private var label: TextView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Log.d("MainActivity", "onCreate")
        setContentView(R.layout.activity_main)

        label = findViewById(R.id.label) as TextView
        val button = findViewById(R.id.button) as Button

        myHandler = lastCustomNonConfigurationInstance as? FibonacciHandler

        if (savedInstanceState != null) {
            startingPos = savedInstanceState.getLong("pos")
        }

        button.setOnClickListener {
            if (myHandler == null) {
                Log.d("MainActivity", "creating new handler")
                myHandler = FibonacciHandler().also { it.startCounting(label!!, startingPos) }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d("MainActivity", "onResume")
        if (startingPos > 0) {
            if (myHandler == null) {
                myHandler = FibonacciHandler()
                Log.d("MainActivity", "creating new handler")

            }
            myHandler!!.startCounting(label!!, startingPos)
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        Log.d("MainActivity", "onSaveInstanceState")
        if (myHandler != null) {
            myHandler!!.stopCounting()
        }
        outState.putLong("pos", myHandler!!.position)
    }

    override fun onStop() {
        super.onStop()
        Log.d("MainActivity", "onStop")
        myHandler!!.stopCounting()
        startingPos = myHandler!!.position
    }

    override fun onRetainCustomNonConfigurationInstance(): Any? {
        Log.d("MainActivity", "onRetainCustomNonConfigurationInstance")
        return myHandler
    }

    override fun onDestroy() {
        super.onDestroy()
        if (!isChangingConfigurations && isFinishing) {
            myHandler!!.cleanUp()
        }
    }
}
